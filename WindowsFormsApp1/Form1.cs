﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Card> deck = new List<Card>();

        private void Form1_Load(object sender, EventArgs e)
        {
            CreateDeck();
            Shuffle();
            foreach (Card c in deck)
            {
                Console.WriteLine(c.Name + ", " + c.Value);
            }
        }

        private void CreateDeck()
        {
            string[] suits = { "hearts", "spades", "diamonds", "clubs" };
            byte[] values = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10 };
            string[] names = { "ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King" };

            foreach(string s in suits)
            {
                for(int i = 0; i<=12;i++)
                {
                    Card card = new Card();
                    card.Suit = s;
                    card.Value = values[i];
                    card.Name = names[i];
                    
                    if (values[i] == 1)
                    {
                        card.Ace = true;
                    }
                    else
                    {
                        card.Ace = false;
                    }

                    deck.Add(card);
                }
            }
        }

        private void Shuffle()
        {
            Random r = new Random();
            int shuffleamount = 100000;

            for(int i = 0; i<= shuffleamount; i++)
            {
                Card temp = new Card();
                int card1 = r.Next(deck.Count()-1);
                int card2 = r.Next(deck.Count()-1);

                temp = deck[card1];
                deck[card1] = deck[card2];
                deck[card2] = temp;
            }
        }
    }
}
